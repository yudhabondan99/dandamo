package com.neherrin.dev.controller;

import com.example.dandamo.conf.KafkaProducerConfig;
import com.example.dandamo.dandamo.model.TestKafk;
import com.neherrin.dev.db.CouchbaseSDK;
import com.neherrin.dev.model.BaseResponse;
import com.neherrin.dev.model.wrapper.PsuRecomendRequest;
import com.neherrin.dev.model.wrapper.PsuRecomendResponse;
import com.neherrin.dev.service.impl.PsuRecomendServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;

@RestController
@RequestMapping("/api/v1/neherrin")
public class PsuRecomendController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CouchbaseSDK.class);
    @Autowired
    private PsuRecomendServiceImpl psuRecomendService;

    @Value(value = "${test.topic.name}")
    private String testTopicName;
    private KafkaProducerConfig kafkaProducerService;

    @Autowired
    private KafkaTemplate<String, TestKafk> testKafkaTemplate;

    @Autowired
    public void MessageController(KafkaProducerConfig kafkaProducerService) {
        this.kafkaProducerService = kafkaProducerService;
    }

    public PsuRecomendController(KafkaProducerConfig kafkaProducerService) {
        this.kafkaProducerService = kafkaProducerService;
    }



    @PostMapping(value = "/recom",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<PsuRecomendResponse> psuRecomend(CouchbaseSDK cbConn,@RequestBody PsuRecomendRequest request){
        PsuRecomendResponse psuRec= psuRecomendService.psuRecomend(cbConn,request).getData();
        BaseResponse<PsuRecomendResponse> response = new BaseResponse<PsuRecomendResponse>();
        testKafkaTemplate.send(testTopicName, response.toString());
        return response;
    }

}
