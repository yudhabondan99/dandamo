package com.example.dandamo.dandamo.controller;

import com.example.dandamo.conf.KafkaProducerConfig;
import com.example.dandamo.dandamo.model.TestKafk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/satu")
public class Satu {

    @Value(value = "${test.topic.name}")
    private String testTopicName;
    private KafkaProducerConfig kafkaProducerService;

    @Autowired
    private KafkaTemplate<String, TestKafk> testKafkaTemplate;

    @Autowired
    public void MessageController(KafkaProducerConfig kafkaProducerService) {
        this.kafkaProducerService = kafkaProducerService;
    }

    public Satu(KafkaProducerConfig kafkaProducerService) {
        this.kafkaProducerService = kafkaProducerService;
    }

    public Satu() {

    }


    @PostMapping("/sa")
    public void postMessage(TestKafk message) {
        testKafkaTemplate.send(testTopicName, message);
    }
}
