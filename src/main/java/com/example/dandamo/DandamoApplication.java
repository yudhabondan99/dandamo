package com.example.dandamo;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Properties;

@SpringBootApplication
public class DandamoApplication {

	public static void main(String[] args) {
		// Set up producer properties
//		Properties properties = new Properties();
//		properties.put("bootstrap.servers", "localhost:9092");
//		properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//		properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//
//		// Create Kafka producer
//		Producer<String, String> producer = new KafkaProducer<>(properties);
//
//		// Topic to which the message will be sent
//		String topic = "test";
//
//		// Message to be sent
//		String message = "Hello, Kafka!";
//
//		// Produce the message
//		ProducerRecord<String, String> record = new ProducerRecord<>(topic, message);
//		producer.send(record);

		// Close the producer
//		producer.close();
		SpringApplication.run(DandamoApplication.class, args);
	}

}
