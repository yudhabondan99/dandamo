package com.neherrin.dev.utility;

public class CustomException extends exception{
    private static final long serialVersionUID = 1L;

    private Integer code;
    private String errorType;
    private String taskName;
    private CouchbaseSDK cbConn;
    private String errorMsg;
    private JsonArray errorStacktrace;
    private Throwable exception;
    private final Gson gson = new Gson();
    private JsonObject responseMsg;
    private String trackId;

    public CustomException(CouchbaseSDK cbConn, Integer code, String errorType, String taskName,Throwable e) {
        super(e);
        this.cbConn = cbConn;
        this.code = code;
        this.errorType = errorType;
        this.exception = e;
        this.taskName = taskName;
        this.trackId = UUID.randomUUID().toString();
        this.responseMsg = initResponseMsg();

    }

    public JsonObject getResponseMsg() {
        return this.responseMsg;
    }

    private JsonObject initResponseMsg() {
        JsonArray stackTrace = gson.fromJson(gson.toJson(exception.getStackTrace()), JsonArray.class);
        JsonArray getStackTrace = new JsonArray();
        if (stackTrace.size() > 20) {
            for (int i = 0; i < 20; i++) {
                getStackTrace.add(stackTrace.get(i));
            }

        }
        JsonObject responseMsg = new JsonObject();

        this.errorStacktrace = stackTrace;

        responseMsg.addProperty("errorCode", this.code);
        responseMsg.addProperty("errorType", this.errorType);
        responseMsg.addProperty("errorMsg", this.errorMsg);
        responseMsg.addProperty("trackId", this.trackId);
        responseMsg.add("errorStacktrace", this.errorStacktrace);

        return responseMsg;
    }

    public void doLog() throws Exception {

        ExceptionUtils exceptionUtils = new ExceptionUtils(this.cbConn, this.errorStacktrace, this.errorType, this.errorMsg, this.taskName);
        exceptionUtils.log();

    }
}
