package com.neherrin.dev.constants;

public class DBConstants {

    public static final String CB_DOCID_KEY = "NEHERRIN";

    //table_name
    public static final String TBL_HEADER = "neherrin_header_tbl";
    public static final String TBL_DETAIL_NEHERRIN = "neherrin_detail_tbl";
    public static final String TBL_DETAIL_LOG = "neherrin_log_tbl";
    //couchbase meta id
    public static final String CB_NEHERRIN_HEADER = "NEHERRIN_HEADER";
    public static final String CB_NEHERRIN_DETAIL = "NEHERRIN_DETAIL";
    public static final String CB_NEHERRIN_LOG = "NEHERRIN_LOG";

    //META ID
    public static final String META_ID_HEADER = CB_DOCID_KEY + "::" + CB_NEHERRIN_HEADER + "::";
    public static final String META_ID_DETAIL = CB_DOCID_KEY + "::" + CB_NEHERRIN_DETAIL + "::";
    public static final String META_ID_LOG = CB_DOCID_KEY + "::" + CB_NEHERRIN_LOG + "::";
}
