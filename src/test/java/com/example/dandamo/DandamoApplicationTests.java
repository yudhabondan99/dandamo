package com.example.dandamo;

import com.example.dandamo.dandamo.controller.Satu;
import com.example.dandamo.dandamo.model.TestKafk;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DandamoApplicationTests {

	@Test
	void contextLoads() {
	}
	TestKafk message;

	@Autowired
	private KafkaTemplate<String, TestKafk> testKafkaTemplate;


	@Test
	void testKafka() {
		Satu satu = new Satu();
		String result = satu.postMessage("test",message);
		assertEquals(message, result);

	}

}
